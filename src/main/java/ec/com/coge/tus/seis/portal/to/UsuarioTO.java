package ec.com.coge.tus.seis.portal.to;

public class UsuarioTO {

	private Integer idUsuario;
	
	private String numeroDocumento;
	
	private String primerNombre;
	
	private String segundoNombre;
	
	private String primerApellido;
	
	private String segundoApellido;
	
	private Integer puntosFidelidad;
	
	private Boolean generaRecompensa;
	
	private String pseudonimo;
	
	private String password;
	
	private Boolean contactoEquipo;

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getPrimerNombre() {
		return primerNombre;
	}

	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}

	public String getSegundoNombre() {
		return segundoNombre;
	}

	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public Integer getPuntosFidelidad() {
		return puntosFidelidad;
	}

	public void setPuntosFidelidad(Integer puntosFidelidad) {
		this.puntosFidelidad = puntosFidelidad;
	}

	public Boolean getGeneraRecompensa() {
		return generaRecompensa;
	}

	public void setGeneraRecompensa(Boolean generaRecompensa) {
		this.generaRecompensa = generaRecompensa;
	}

	public String getPseudonimo() {
		return pseudonimo;
	}

	public void setPseudonimo(String pseudonimo) {
		this.pseudonimo = pseudonimo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getContactoEquipo() {
		return contactoEquipo;
	}

	public void setContactoEquipo(Boolean contactoEquipo) {
		this.contactoEquipo = contactoEquipo;
	}
	
	
}
