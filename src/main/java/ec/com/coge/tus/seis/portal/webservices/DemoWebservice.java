package ec.com.coge.tus.seis.portal.webservices;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ec.com.coge.tus.seis.core.dto.UsuarioDTO;
import ec.com.coge.tus.seis.core.servicio.IUsuarioServicio;
import ec.com.coge.tus.seis.portal.to.UsuarioTO;
import ec.com.coge.tus.seis.portal.util.DemoUtil;

@Controller // This means that this class is a Controller
@RequestMapping(path = "/demo") // This means URL's start with /demo (after Application path)
public class DemoWebservice {

	@Autowired
	private IUsuarioServicio usuarioServicio;
	@Autowired
	private DemoUtil loginUtil;
	
	@RequestMapping(path = "/obtenerUsuarios", method = RequestMethod.GET)
	public ResponseEntity<Collection<UsuarioTO>> todos() {
		Collection<UsuarioDTO> userDetails = usuarioServicio.obtenerUsuarios();
		Collection<UsuarioTO> response = loginUtil.convertirUsuariosDTO(userDetails);
		return new ResponseEntity<Collection<UsuarioTO>>(response, HttpStatus.OK);
	}
}
